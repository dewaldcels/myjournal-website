const express = require('express');
const path = require('path');
const app = express();
const { PORT = 3001 } = process.env;

app.use( '/public', express.static( path.join( __dirname, 'www' ) ) );

app.get('/', (req, res) => {
    return res.redirect('/journals');
});

app.get('/journals', (req, res) => {
    return res.sendFile( path.join( __dirname, 'www', 'index.html' ) );
})

app.get('/entries', (req, res) => {
    return res.sendFile( path.join( __dirname, 'www', 'entries.html' ) );
});

app.get('/create-entry', (req, res) => {
    return res.sendFile( path.join( __dirname, 'www', 'create-entry.html' ) );
});

app.get('/tags', (req, res) => {
    return res.sendFile( path.join( __dirname, 'www', 'tags.html' ) );
});

app.listen(PORT, ()=> console.log('Website is serving...'));