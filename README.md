# MyJournal Website

A simple website written in HTML, CSS and JavaScript. The website demonstrates how we can use the technologies to consume an API. 

## Install
To install the dependencies for the project you must run the following in the root directory of the project.

```bash
npm install 
```

## Run
To run the application you must execute the following command in the terminal in the root directory of the project. It will run a node server that serves the content in the www folder.

```bash
npm start 
```

