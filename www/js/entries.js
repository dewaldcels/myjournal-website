function Entries() {
    this.elEntries = document.getElementById('entries');
    this.entries = [];
}

Entries.prototype.init = async function() {
    try {

        this.entries = await entry(fetch).get().then(r => r.data);

        this.render();
    } catch (e) {
        console.error(e);
    }
}

Entries.prototype.render = function() {
    this.elEntries.innerHTML = '';
    this.entries.forEach(entry => {

        const elEntry = document.createElement('div');
        const elTitle = document.createElement('h4');
        const elContent = document.createElement('p');
        const elCreated = document.createElement('small');

        elEntry.id = entry.journal_entry_id;
        elEntry.className = 'journal-entry';
        elTitle.innerText = entry.title;
        elContent.innerText = entry.content;
        elCreated.innerText = entry.created_at;

        elEntry.appendChild(elTitle);
        elEntry.appendChild(elContent);
        elEntry.appendChild(elCreated);

        this.elEntries.appendChild(elEntry);
    });
}

const appEntries = new Entries();
appEntries.init();