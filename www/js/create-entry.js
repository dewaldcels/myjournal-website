function CreateEntry() {
    // Preloaded data
    this.journals = [];
    this.authors = [];
    this.tags = [];

    // New Entry data
    this.entryAuthor = -1;
    this.entryJournal = -1;
    this.entryTags = [];
    this.entry = null;
    this.tagSearchText = '';

    this.elJournals = document.getElementById('journals');
    this.elAuthors = document.getElementById('authors');
    this.elEntryForm = document.getElementById('entry-form');
    this.elSearchTags = document.getElementById('search-tags');
    this.elSelectedTags = document.getElementById('selected-tags');
    this.elAvailableTags = document.getElementById('available-tags');
    this.elPublishEntry = document.getElementById('btn-publish');
}

CreateEntry.prototype.attachListeners = function () {
    this.elJournals.addEventListener('change', this.onJournalChange.bind(this));
    this.elAuthors.addEventListener('change', this.onAuthorChange.bind(this));
    this.elSearchTags.addEventListener('focus', this.onSearchFocus.bind(this));
    this.elSearchTags.addEventListener('blur', _.debounce(this.onSearchBlur.bind(this), 250));
    this.elSearchTags.addEventListener('keyup', _.debounce(this.onSearchType.bind(this), 500));
    this.elPublishEntry.addEventListener('click', this.onPublishClicked.bind(this));
}

CreateEntry.prototype.init = async function () {
    try {
        this.journals = await journal(fetch).get().then(r => r.data);
        this.authors = await author(fetch).get().then(r => r.data);
        this.tags = await tag(fetch).get().then(r => r.data);
        this.render();
    } catch (e) {
        console.error(e);
    }
};

CreateEntry.prototype.render = function () {

    if (this.entryJournal == -1) {
        this.elJournals.innerHTML = '';
        this.journals.forEach(journal => {
            const elJOption = document.createElement('option');
            elJOption.innerText = journal.name;
            elJOption.value = journal.journal_id;
            this.elJournals.appendChild(elJOption);
        });
    }

    if (this.entryAuthor == -1) {
        this.elAuthors.innerHTML = '';
        this.authors.forEach(author => {
            const elAOption = document.createElement('option');
            elAOption.innerText = author.first_name + ' ' + author.last_name;
            elAOption.value = author.author_id;
            this.elAuthors.appendChild(elAOption);
        });
    }

    this.elAvailableTags.innerHTML = '';
    this.tags
        .filter(t => !this.entryTags.map(t => t.name).join(',').toLowerCase().includes(t.name))
        .filter(t => t.name.toLowerCase().includes(this.tagSearchText.toLocaleLowerCase()))
        .forEach(tag => {
            const elTag = document.createElement('li');
            elTag.id = tag.tag_id;
            elTag.innerText = tag.name;
            elTag.style.display = 'block';
            elTag.addEventListener('click', e => {
                this.entryTags.push(tag);
                this.render();
            });
            this.elAvailableTags.appendChild(elTag);
        });

    this.elSelectedTags.innerHTML = '';
    this.entryTags.forEach(tag => {
        const elTag = document.createElement('span');
        elTag.id = tag.tag_id;
        elTag.innerText = tag.name;
        elTag.className = 'selected-tag';
        this.elSelectedTags.appendChild(elTag);
    });

    if (this.tagSearchText) {
    }
}

CreateEntry.prototype.onSearchFocus = function (e) {
    this.elAvailableTags.style.display = 'block';
}

CreateEntry.prototype.onAuthorChange = function (e) {
    const { value } = e.target;
    this.entryAuthor = value;
    this.render();
}

CreateEntry.prototype.onJournalChange = function (e) {
    const { value } = e.target;
    this.entryJournal = value;
    this.render();
}

CreateEntry.prototype.onSearchBlur = function (e) {
    this.elAvailableTags.style.display = 'none';
}

CreateEntry.prototype.onSearchType = function (e) {
    const { value } = e.target;
    this.tagSearchText = value.trim();
    this.render();
}

CreateEntry.prototype.onPublishClicked = async function (e) {
    const { elements } = this.elEntryForm;
    this.entry = {
        title: elements['entryTitle'].value || null,
        content: elements['entryContent'].value || null,
        author_id: this.elAuthors.value,
        journal_id: this.elJournals.value
    };

    try {
        const newEntry = await entry(fetch).create(this.entry).then(r => r.data);
        const linkTagResult = await entryTag(fetch).create(newEntry.journal_entry_id, this.entryTags.map(t => t.tag_id));

        if (newEntry.journal_entry_id) {
            alert('Succesfully published new entry');
            this.clearEntryForm();
        } else {
            alert('Could not add new Journal Entry');
        }

    } catch (e) {
        console.log(e);
    }
}

CreateEntry.prototype.clearEntryForm = function () {
    Object.keys(this.elEntryForm.elements).forEach(key => this.elEntryForm.elements[key].value = '');
    this.elAuthors.value = -1;
    this.elJournals.value = -1;
    this.entry = null;
    this.entryTags = [];
    this.entryJournal = -1;
    this.entryAuthor = -1;
}

const appCreateEntry = new CreateEntry();
appCreateEntry.attachListeners();
appCreateEntry.init();