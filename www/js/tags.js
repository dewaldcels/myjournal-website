function Tags() {
    this.elTags = document.getElementById('tags');
    this.tags = [];
}

Tags.prototype.init = async function() {
    try {
        this.tags = await tag(fetch).get().then(r => r.data);
        this.render();
    } catch (e) {
        console.log(e);
    }
}

Tags.prototype.render = function() {
    this.tags.forEach(tag => {
        const elTag = document.createElement('div');
        const elTName = document.createElement('span');
        const elTCreated = document.createElement('span');
        const elTActions = document.createElement('div');
        const elTEdit = document.createElement('button');
        const elTDelete = document.createElement('button');
 
        elTag.className = 'tag';
        elTName.className = 'tag-name';
        elTCreated.className = 'tag-created';
        elTDelete.className = 'btn-delete';
        
        elTName.innerText = tag.name;
        elTCreated.innerText = tag.created_at;
        elTEdit.innerText = 'Edit'
        elTDelete.innerText = 'Delete'

        elTag.appendChild(elTName);
        elTag.appendChild(elTCreated);
        elTActions.appendChild(elTEdit);
        elTActions.appendChild(elTDelete);
        elTag.appendChild(elTActions);

        this.elTags.appendChild(elTag);
    });
}

const appTags = new Tags();
appTags.init();