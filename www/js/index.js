function App() {
    this.journals = [];;
    this.elJournals = document.getElementById('journals')
}

App.prototype.init = function() {
    this.getAllJournals();
};

App.prototype.render = function() {
    this.journals.forEach(journal => {

        const elJournal = document.createElement('div');
        const elJName = document.createElement('h4');
        const elJCreated = document.createElement('small');

        elJournal.className = 'journal';
        elJName.innerText = journal.name;
        elJCreated.innerText = 'Created on: ' + journal.created_at;

        elJournal.appendChild(elJName);
        elJournal.appendChild(elJCreated);
        this.elJournals.appendChild(elJournal);
    });
}

App.prototype.getAllJournals = async function() {
    try {
        const result = await journal(fetch).get().then(r => r.data);
        this.journals = result;
        this.render();
    } catch (e) {
        console.error(e);
    }
}

App.prototype.getFirstJournal = async function() {
    try {
        const result = await journal(fetch).getById(1);
        console.log('First Journal', result);
    } catch (e) {
        console.error(e);
    }
}


const app = new App();
app.init();