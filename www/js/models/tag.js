const tag = fetch => ({
    get: () => fetch('http://localhost:3000/v1/api/tags').then(r => r.json()),
    getById: id => fetch(`http://localhost:3000/v1/api/tags/${id}`).then(r => r.json())
})