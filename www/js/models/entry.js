const entry = fetch => ({
    get: () => fetch('http://localhost:3000/v1/api/entries').then(r => r.json()),
    getById: (id) => fetch(`http://localhost:3000/v1/api/entries/${id}`).then(r => r.json()),
    create: entry => {
        return fetch('http://localhost:3000/v1/api/entries', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                entry: entry
            })
        }).then(r => r.json())
    }
});