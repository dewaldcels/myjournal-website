const author = fetch => ({
    get: () => fetch('http://localhost:3000/v1/api/authors').then(r => r.json()),
    getById: id => fetch(`http://localhost:3000/v1/api/authors/${id}`).then(r => r.json()),
});