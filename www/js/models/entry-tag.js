const entryTag = (fetch) => ({
    create: (journal_entry_id, tags) => {
        return fetch('http://localhost:3000/v1/api/entry-tags', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                journal_entry_id,
                tags
            })
        })
    }
});