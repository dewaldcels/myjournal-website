const journal = (fetch) => ({
    get: () => fetch('http://localhost:3000/v1/api/journals').then(r => r.json()),
    getById: (id) => fetch(`http://localhost:3000/v1/api/journals/${id}`).then(r => r.json()),
    getByIdWithEntries: (id) => fetch(`http://localhost:3000/v1/api/journals/${id}/entries`).then(r => r.json())
})